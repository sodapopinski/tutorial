var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('underscore');
var check_valid = function(value){
   var not_valid = /[\/\\:&?.#@]/;
   if (value.search(check_valid)==-1)return true;
   else return false;
  return true;
}
var userSchema = new Schema({
    isAdmin: {
      type: Boolean,
      //match: /guest|author|admin/
    },
    isValid: {type : Boolean, default : false},
    login: {type: String, index: {unique : true}},
    email: {type: String},
    password: String,
    messages: [{type: mongoose.Schema.ObjectId, ref: 'messages'}]
  });
var sectionSchema = new Schema({
    name: {type : String, index:{unique : true}},
    topics: [
      {type: String}
    ]
  });
var topicSchema = new Schema({
    name: {type: String, index:{unique : true}},
    messages: [
      {type: mongoose.Schema.ObjectId, ref: 'messages'}
    ]
  });
var messageSchema = new Schema({
    author : String,
    text : String,
    timestamp : {type: Date, default : Date.now},
  });
topicSchema.post('remove', function( doc){
      _.each(doc.messages,function(id){
        Message.findById(id).remove().exec();
      })
    });

var Message = mongoose.model("messages", messageSchema);
var User = mongoose.model("users", userSchema);
var Section = mongoose.model("sections", sectionSchema);
var Topic = mongoose.model("topics", topicSchema);
var db_model = {
  addUser : function(isAdmin, isValid, login, email, password, callback) {
      new User({
      isAdmin: isAdmin,
      isValid: isValid,
      login: login,
      email: email,
      password: password
    }).save(function(err, newUser) {
      if (err) {
        console.log(err);
        callback(false);
      }
      else{
        callback(true);
      }
    });
  },
  login : function(login, password, callback){
    User.findOne({login: login, password: password, isValid: true}, function(err, doc){
      if(err){
        console.log(err);
      }
      else{
        if(doc == null) callback(null, null);
        else callback(doc._id, doc.isAdmin);
      }
    })
  },
  deleteUser : function(login, whoTry) {
    User.findOne({
      login: login
    }, function(err, doc){
      if(err)console.log(err);
      else{
        _.each(doc.messages,function(id){
          Message.findById(id).remove().exec();
        });
      }
    }).remove().exec();
  },

  addTopic : function(name, section, callback) {
    if(!check_valid(name)){
        callback(false);
    }
    else{
      new Topic({
        name: name,
        messages: [],
        section: section
      }).save(function(err, topic) {
        if (err) {
          console.log(err);
        }
        else{
          Section.findOne({name : section},function(err, section){
            section.topics.push(topic.name);
            section.save(function(err){
              if(err){
                console.log(err);
              }
              else{
                callback(true)
              }
            });        
          });
        }
      });
    }
  },
  addMessage : function(text, topic, author) {
    //console.log(author);
    new Message({
      text: text,
      author: author,
    }).save(function(err,message) {
      if (err) {
        console.log(err);
      }
      else{
        Topic.findOne({name : topic},function(err, topic){
          topic.messages.push(message);
          topic.save();        
        });
        User.findOne({login : author},function(err, user){
          user.messages.push(message);
          user.save();        
        });
      }
    });
  },
  watchMessages : function(topic,callback){
    Topic.findOne({name: topic},function(err,topic){
      if (topic == null)
      {
        callback(false);
      }
      else{
        if(err){
          console.log(err)
        }
        else{
          Message.find({_id: {$in: topic.messages}}, function(err, messages){
            if(err){
              console.log(err);
            }
            else callback(messages);
          });
        }
      }
    });
  },
  deleteMessage : function(id, topic, author, whoTry) {
    console.log(id, topic, author, whoTry)
    Message.findById(id).remove().exec();
    Topic.findOne({name: topic},function(err,doc){
      _.without(doc.messages, id);
      doc.save();
    });
    User.findOne({login: author},function(err,doc){
      _.without(doc.messages, id);
      doc.save();
    });
  },

  addSection : function(name, callback) {
    if(!check_valid(name)){
        callback(false);
    }
    else{
      var newSection;
      newSection = new Section({
        name: name,
        topics: []
      }).save(function(err, newSection) {
        if (err) {
          console.log(err);
        }
        else callback(true);
      });
    }
  },
  watchSections : function(callback){
    Section.find({},function(err, docs){
      callback(_.pluck(docs, 'name'));
    });
  },
  deleteSection : function(name, whoTry) {
    Section.findOne({
      name: name
    },function(err,doc){
      if(err){
        console.log(err);
      }
      else{
        _.each(doc.topics,function(name){
          Topic.findOne({name: name}).remove().exec();
        });
      }
    }).remove().exec();
  },
  watchTopics : function(section, callback){
    Section.findOne({name:section}, function(err, doc){
      if(doc == null){
        callback(false);
      }
      else callback(doc.topics);})
  },
  deleteTopic : function(name, section, whoTry) {
    console.log(name);
    Topic.remove({name: name},function(){
      Section.findOne({name: section},function(err, doc){
        doc.topics = _.without(doc.topics, name);
        doc.save();
      });
    })
  },
  userExist : function(login,callback){
    User.findOne({login: login},function(err, doc){
      if(doc === null) callback(false);
      else callback(true);
    })
  }
}
exports.db_model = db_model;