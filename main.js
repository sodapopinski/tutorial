var express         = require('express')
var path            = require('path')
var bodyParser      = require('body-parser')
var cookieParser    = require('cookie-parser')
var session         = require('cookie-session')
var crypto          = require('crypto')
var db_model        = require('./db_connect').db_model
var mongoose        = require('mongoose')

mongoose.connect('mongodb://localhost/forum')

var port = 1337
var app = express()

app.set('view engine', 'jade')

app.use(express.static(path.join(__dirname, 'static')))
app.use(bodyParser.urlencoded({extended:true}))

app.use(cookieParser())
app.use(session({secret:'mySecret1337'}))
// app.use(function (req, res, next) {
//   db_model.userExist(req.session.nickname,function(ex){
//     if(!ex) req.session = null;
//   })
//   next();
// });

// // Админка
// db_model.addUser(true, true, "admin", "admin@admin.admin", "admin", function(success){
//     console.log("Admin account created")
// })

app.get('/:topic?/delete_user/:login', function (req, res) {
    db_model.deleteUser(req.params.login, req.session.nickname)
    // console.log('deleteUser')
    res.redirect(req.get('referer'))
})

app.get('/delete_section/:section', function (req, res) {
    db_model.deleteSection(req.params.section, req.session.nickname)
    res.redirect(req.get('referer'))
})

app.get('/delete_topic/:section/:topic', function (req, res) {
    // console.log(req.params.topic +' '+ req.params.section +' '+ req.session.nickname)
    db_model.deleteTopic(req.params.topic, req.params.section, req.session.nickname)
    res.redirect(req.get('referer'))
})

app.get('/:section/delete_message/:_id/:author/:topic', function (req, res) {
    console.log(req.params._id + ' '+ req.params.topic +' '+ req.params.author +' '+ req.session.nickname)
    db_model.deleteMessage(req.params._id, req.params.topic, req.params.author, req.session.nickname)
    res.redirect(req.get('referer'))
})

app.get('/logout', function (req, res) {
    req.session = null
    console.log("logout")
    res.redirect('/')
});

app.get('/:top?/:topic?/', function(req, res) {
    
    db_model.userExist(req.session.nickname,function(ex){
      if(!ex) req.session = null;
    })
    
    var args = {
        isAdmin:                false,
        registered:             false,
        // dirname:                "localhost:"+port
    }

    if(req.session.user_id)
    {
        args.isAdmin = req.session.isAdmin
        args.registered = true
        args.nickname = req.session.nickname
        console.log(args)
    }

    // var path_to_registration = 'registration'
    // var path_to_section
    // var path_to_topic

    var view = 'homepage'
    var top = req.params.top
    var topic = req.params.topic

    if (typeof top === "undefined" || top === null) {
        // homepage
        view = 'homepage'
        db_model.watchSections(function (sections){
            args.sections = sections
            // console.log(args)
            res.render(view, args)
        })

        // console.log('homepage')
    }
    else if (top == "registration") {
        // registration
        view = 'registration'
        args.path_to_registration = 'registration'
        res.render(view, args)
        // console.log('registration')
    }
    else if (typeof topic === "undefined" || topic === null) {
        // section
        view = 'homepage'
        // console.log(top)
        db_model.watchTopics(top, function (topics){
            args.path_to_section = top
            args.topics = topics
            // console.log(args)
            res.render(view, args)
        })
    }
    else {
        // topic
        view = 'topic'
        db_model.watchMessages(topic, function (messages){
            args.messages = messages
            args.path_to_section = top
            args.path_to_topic = topic
            // console.log(args)
            res.render(view, args)
        })
        // console.log('topic')
    }
})

app.post('/registration', function(req, res){
    isAdmin = false
    registered = true
    // registered = false
    var post = req.body;
    db_model.addUser(isAdmin, registered, post.login, post.email, post.password, function(success){
        if (success) console.log("successful registration")
    })
    res.redirect('/')
})

app.post('/login', function (req, res) {
    var post = req.body;
    db_model.login(post.login, post.password, function(user_id, isAdmin){
        console.log("user_id="+user_id+" isAdmin="+isAdmin)
        if(user_id!=null) {
            // console.log("session created")
            req.session.user_id = user_id
            req.session.isAdmin = isAdmin
            req.session.nickname = post.login
            res.redirect(req.get('referer'))
        }
    })
})

app.post('/new_section', function (req, res) {
    var post = req.body
    db_model.addSection(post.section, function (success){
        if (success) console.log("success")
    })
    res.redirect(req.get('referer'))
})

app.post('/new_topic/:section', function (req, res) {
    var post = req.body
    db_model.addTopic(post.topic, req.params.section, function(success){
        ;
    })
    db_model.addMessage(post.message, post.topic, req.session.nickname)
    // console.log(post)
    res.redirect(req.get('referer'))
})

app.post('/new_message/:section/:topic', function (req, res) {
    var post = req.body
    db_model.addMessage(post.text, req.params.topic, req.session.nickname)
    res.redirect(req.get('referer'))
})

app.listen(port, function(){
    console.log("Server listening on port " + port )
})